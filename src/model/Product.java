package model;

import java.util.concurrent.atomic.AtomicInteger;

public class Product {
    private int id;
    private String name;
    private double unitPrice;
    private int qty;
    private String date;

    public Product(){

    }

    public Product(int id, String name, double unitPrice, int qty, String date){
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toString(){
       return (this.getId() + this.getName() + this.getUnitPrice() + this.getQty() + this.getDate());
    }
}
