package UserInterface;

import com.company.Display;
import java.util.Scanner;

public class Main {

    private static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        Display.header();
        Display.menu();
        Display.chooseOpt();
    }
}
