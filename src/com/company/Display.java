package com.company;
import Database.AccessDatabase;
import Database.DBUtil;
import model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.*;
import org.nocrala.tools.texttablefmt.Table;

import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Display {
    private static Scanner reader = new Scanner(System.in);
    public static String inputOption;
    static int pagesize=3;
    static Connection c = DBUtil.connectDB();
    static ArrayList<Product> ar = AccessDatabase.getDataIntoArray(c, "product");
    static Pagination<Product> pro = new Pagination<>(ar,pagesize);
    static ArrayList<Product> tempArray = ar;

    static String[] arrWaiting= {"P","l","e","a","s","e"," ","W","a","i","t","!"};
    static String[] arrLoading={"L","o","a","d","i","n","g",".",".",".","."};
    static String[] arrCurrentTime={"C","u","r","r","e","n","t"," ","L","o","a","d","i","n","g"," ",":"," ","3"};
    //Function Executing Thread to Sleep for 500 milliseconds
    public static void sleep(){
        try{
            Thread.sleep(200);
        }catch (Exception e){

        }
    }
    //Template Function to Run Loop
    public static void printTemplate(String[] pr){
        for(int i=0; i<pr.length; i++){
            Display.sleep();
            System.out.print(pr[i]);
        }
    }
    public static void header() {
        System.out.println("                        Welcome to                         ");
        Display.sleep();
        System.out.println("                     Stock Management                 ");
        Display.sleep();
        System.out.println();
        System.out.println("\n" +
                "  ____            _     _                         _                                _____   __ \n" +
                " |  _ \\          | |   | |                       | |                              / ____| /_ |\n" +
                " | |_) |   __ _  | |_  | |_    __ _   _ __ ___   | |__     __ _   _ __     __ _  | |  __   | |\n" +
                " |  _ <   / _` | | __| | __|  / _` | | '_ ` _ \\  | '_ \\   / _` | | '_ \\   / _` | | | |_ |  | |\n" +
                " | |_) | | (_| | | |_  | |_  | (_| | | | | | | | | |_) | | (_| | | | | | | (_| | | |__| |  | |\n" +
                " |____/   \\__,_|  \\__|  \\__|  \\__,_| |_| |_| |_| |_.__/   \\__,_| |_| |_|  \\__, |  \\_____|  |_|\n" +
                "                                                                           __/ |              \n" +
                "                                                                          |___/               \n");

        printTemplate(arrWaiting);
        System.out.println();
        printTemplate(arrLoading);
        System.out.println();
        printTemplate(arrCurrentTime);
        System.out.println();

        //call recovery fucntion
        if(!Bonus.checkIfTempNull(c)){
            String input = Validator.inputYesNo(new Scanner(System.in),
                    "Would you like to recover your unsaved data? (Y/y) (N/n) : ");
            if(input.toLowerCase().equals("y")){
                AccessDatabase.deleteAll(c, "product");
                AccessDatabase.insertIntoOtherTable(c, "product", "temp");
                ar = AccessDatabase.getDataIntoArray(c, "temp");
            }
            pro = new Pagination<>(ar,pagesize);
        }

    }

    public static void menu(){

        System.out.println("==============================================================================================================\n" +
                "| *)Display   | W)rite    | R)ead     | U)pdate  | D)elete   | F)irst     | P)revious   | N)ext   | L)ast  \n" +
                "| S)earch     | G)o To    | Se)t Row  | Sa)ve    | Ba)ckup   | Re)store   | H)elp       | E)xit\n" +
                "==============================================================================================================\n");
    }

    public static void chooseOpt(){
        while (true){
            System.out.print("Please choose an Option : ");
            inputOption=reader.nextLine();
            String inOp=  inputOption.toUpperCase();

            switch(inOp) {
                case "*":
                    List<Product> pr = pro.displayHome();
                    Display.display(ar,pr);
                    menu();
                    break;
                case "W":
                    Display.writeData(ar);
                    tempArray = ar;
                    menu();
                    pro = new Pagination<>(ar,pagesize);
                    break;
                case "R":
                    Display.readData(ar);
                    menu();
                    break;
                case "U":
                    DeleteAndUpdate.update(ar);
                    tempArray = ar;
                    menu();
                    break;
                case "D":
                    DeleteAndUpdate.delete(ar);
                    tempArray = ar;
                    menu();
                    pro = new Pagination<>(ar,pagesize);
                    break;
                case "F":
                    List<Product> fir = pro.firstPage();
                    Display.display(ar,fir);
                    menu();
                    break;
                case "P":
                    if(pro.getCurrentPage()==1){
                        List<Product> prev = pro.lastPage();
                        display(ar,prev);
                    }else{
                        List<Product> pre = pro.previousPage();
                        display(ar,pre);
                    }
                    menu();
                    break;
                case "N":
                    if(pro.getCurrentPage()>=pro.numberOfPages()){
                        List<Product> fi = pro.firstPage();
                        display(ar,fi);
                    }else{
                        List<Product> pl = pro.nextPage();
                        display(ar,pl);
                    }
                    menu();
                    break;
                case "L":
                    List<Product> la = pro.lastPage();
                    display(ar,la);
                    menu();
                    break;
                case "S":
                    Search.SearchName(c, ar);
                    break;
                case "G":
                    System.out.print("Go to page: "); String page = reader.nextLine();
                    if(Integer.parseInt(page)>pro.numberOfPages()){
                        List<Product> las = pro.lastPage();
                        display(ar,las);
                    }else if(Integer.parseInt(page)<1){
                        List<Product> firs = pro.firstPage();
                        Display.display(ar,firs);
                    }else{
                        List<Product> go = pro.goTo(Integer.parseInt(page));
                        display(ar,go);
                    }
                    menu();
                    break;
                case "SE":
                    pagesize = Validator.inputNum(new Scanner(System.in), "Please enter row to display: ");
                    Pagination<Product> pp = new Pagination<>(ar,pagesize);
                    pro=pp;
                    menu();
                    break;
                case "SA":
                    Search.save(c, ar);
                    tempArray = null;
                    break;
                case "BA":
                    Bonus.backUp(ar);
                    menu();
                    break;
                case "-10M":
                    Display.addMillionRecord(ar);
                    menu();
                    pro = new Pagination<>(ar,pagesize);
                    break;
                case "RE":
                    Bonus.restore(ar);
                    pro = new Pagination<>(ar,pagesize);
                    menu();
                    break;
                case "H":
                    Display.help();
                    chooseOpt();
                    break;
                case "E":
                    Bonus.checkIfTempArrayNull(tempArray);
                    System.exit(0);
                    break;
                default:
                    if (inputOption.charAt(0)=='w'&& inputOption.charAt(1)=='#'){
                        String result= inputOption.substring(2);
                        String[] split=result.split("-");
                        int id = getLastIndex(ar);
                        try{
                            Product p = new Product(id + 1, split[0], Double.parseDouble(split[1]), Integer.parseInt(split[2]), LocalDate.now().toString());
                            displaySingleItem(p);

                            String input = Validator.inputYesNo(new Scanner(System.in),
                                    "Do you want to add this to your record? [Y/y] or [N/n] : ");
                            if(input.toLowerCase().equals("y")){
                                ar.add(p);
                                tempArray = ar;
                                pro = new Pagination<>(ar,pagesize);
                                System.out.println(p.getId() + " was add successfully");
                            }
                        }catch (Exception e){
                            System.out.println("Error! Please look at shortcut again");
                        }

                        break;
                    }else if(inputOption.charAt(0)=='r'&& inputOption.charAt(1)=='#'){
                        try {
                            String resultRead= inputOption.substring(2);
                            for(Product p : ar){
                                if(p.getId() == Integer.parseInt(resultRead)){
                                    displaySingleItem(p);
                                }
                            }
                        }catch (Exception e){
                            System.out.println("Error! Please look at shortcut again");
                        }

                    }else if (inputOption.charAt(0)=='d'&& inputOption.charAt(1)=='#'){

                        try {
                            String resultDel= inputOption.substring(2);
                            for(Product p : ar){
                                if(p.getId() == Integer.parseInt(resultDel)){
                                    displaySingleItem(p);
                                    String input = Validator.inputYesNo(new Scanner(System.in),
                                            "Do you want to Delete this from your record? [Y/y] or [N/n] : ");
                                }
                                if(resultDel.toLowerCase().equals("y")){
                                    ar.remove(p);
                                    tempArray = ar;
                                    pro = new Pagination<>(ar,pagesize);
                                    System.out.println(p.getId() + " was removed successfully");
                                }
                            }
                        }catch (Exception e){
                            System.out.println("Error! Please look at shortcut again");
                        }

                    }
                    else
                        System.out.println("Invalid Option!\n");;
            }
        }
    }

    public static void display(ArrayList<Product> productList,List<Product> pr){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.center, CellStyle.AbbreviationStyle.crop,
                CellStyle.NullStyle.emptyString);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX_WIDE, ShownBorders.ALL, false, "");
        Table t1 = new Table(2, BorderStyle.DESIGN_CURTAIN_HEAVY, ShownBorders.ALL, false, "");
        t.setColumnWidth(0,20,20);
        t.setColumnWidth(1,20,20);
        t.setColumnWidth(2,20,20);
        t.setColumnWidth(3,20,20);
        t.setColumnWidth(4,20,20);
        t.addCell("ID", cs);
        t.addCell("Name", cs);
        t.addCell("UnitPrice", cs);
        t.addCell("Qty", cs);
        t.addCell("Imported Date", cs);
        for (Product p : pr) {
            t.addCell(Integer.toString(p.getId()), cs);
            t.addCell(p.getName(), cs);
            t.addCell(Double.toString(p.getUnitPrice()));
            t.addCell(Integer.toString(p.getQty()), cs);
            t.addCell(p.getDate(), cs);
        }
        System.out.println(t.render());
        t1.setColumnWidth(0,50,50);
        t1.setColumnWidth(1,50,50);
        t1.addCell("Page: " + pro.getCurrentPage() +"/" + pro.numberOfPages(),cs);
        t1.addCell("Total Record : " + productList.size(),cs);
        System.out.println(t1.render());
    }


    public static void writeData(ArrayList<Product> productList){
        int id = getLastIndex(productList);
        System.out.print("Product's Name : ");
        String name =reader.nextLine();
        Double unitPrice = Validator.inputDouble(new Scanner(System.in), "Product's UnitPrice : ");
        int qty = Validator.inputNum(new Scanner(System.in), "Product's Qty : ");

        Product p = new Product(id + 1, name, unitPrice, qty, LocalDate.now().toString());

        displaySingleItem(p);

        String input = Validator.inputYesNo(new Scanner(System.in),
                "Do you want to add this to your record? [Y/y] or [N/n] : ");
        if(input.toLowerCase().equals("y")){
            productList.add(p);
            System.out.println(p.getId() + " was add successfully");
        }
    }

    public static void addMillionRecord(ArrayList<Product> productList){
        int id = getLastIndex(productList);
        for(int i = 1; i <= 1000000; i++){
            Product p = new Product(id+i , "Coca Cola", 0.5, 1, LocalDate.now().toString());
            productList.add(p);
        }
        System.out.println(productList.size());
    }

    public static void readData(ArrayList<Product> productsList){
        int id = Validator.inputNum(new Scanner(System.in), "Read By ID : " );
        Product p = searchByID(id , productsList);

        if(p != null){
            displaySingleItem(p);
        }else
            System.out.println("This product doesn't exist.");
    }

    public static Product searchByID(int id, ArrayList<Product> productsList) {
        for(Product p : productsList){
            if(p.getId() == id){
                return p;
            }
        }
        return null;
    }

    public static void displaySingleItem(Product p) {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.center, CellStyle.AbbreviationStyle.crop,
                CellStyle.NullStyle.emptyString);
        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX_WIDE, ShownBorders.ALL, false, "");

        System.out.println();
        t.addCell("Product's ID : " + p.getId(), cs);
        t.addCell("Product's Name : " + p.getName(), cs);
        t.addCell("Product's UnitPrice : " + p.getUnitPrice(), cs);
        t.addCell("Product's Qty : " + p.getQty(), cs);
        t.addCell("Product's Imported Date : " + p.getDate(), cs);
        System.out.println(t.render());
        System.out.println();

    }

    private static int getLastIndex(ArrayList<Product> productList) {
        Product p = productList.get(productList.size() - 1);
        return p.getId();
    }

    public static void help() {
        System.out.println("+--------------------------------------------------------------------------------------------+");
        System.out.println("!\t1.    Press      *  : Display all record of products                                     !");
        System.out.println("!\t2.    Press      w  : Add new product                                                    !");
        System.out.println("!\t      Press      w#proname-unitprice-qty : shortcut for add new product                  !");
        System.out.println("!\t3.    Press      r  : read Content any content                                           !");
        System.out.println("!\t      Press      r#proId : shortcut for read product by ID                               !");
        System.out.println("!\t4.    Press      u  : Update Data                                                        !");
        System.out.println("!\t5.    Press      d  : Delete Data                                                        !");
        System.out.println("!\t      Press      d#proId : shortcut for delete product by ID                             !");
        System.out.println("!\t6.    Press      f  : Display First Page                                                 !");
        System.out.println("!\t7.    Press      p  : Display Previous Page                                              !");
        System.out.println("!\t8.    Press      n  : Display Next Page                                                  !");
        System.out.println("!\t9.    Press      l  : Display Last Page                                                  !");
        System.out.println("!\t10.   Press      s  : Search product by name                                             !");
        System.out.println("!\t11.   Press      sa : Save record to file                                                !");
        System.out.println("!\t12.   Press      ba : Backup Data                                                        !");
        System.out.println("!\t13.   Press      re : Restore Data                                                       !");
        System.out.println("!\t14.   Press      h  : Help                                                               !");
        System.out.println("+--------------------------------------------------------------------------------------------+");

    }

}

