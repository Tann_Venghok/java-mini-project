//package com.company;
//import Database.AccessDatabase;
//import model.Product;
//import java.sql.Connection;
//import java.util.ArrayList;
//
//public class Search {
//
//    public static void search(){
//        System.out.println("Search");
//    }
//    public static void help(){}
//
//
//}

package com.company;
import Database.AccessDatabase;
import model.Product;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.Table;

public class Search{
    public static Scanner reader = new Scanner(System.in);

    public static void SearchName(Connection c, ArrayList<Product> productList) {
            ArrayList<Product> result = new ArrayList<Product>();

            System.out.print("Search by Name : ");

            String input = reader.nextLine();


            for (Product p : productList) {
            if (p.getName().contains(input)) {
            result.add(p);
            }
            }
            System.out.println("product found for[" + input + "]:" + result.size());

            Table table = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER);
            table.addCell("Id");
            table.addCell("Name");
            table.addCell("Unit price");
            table.addCell("Qty");
            table.addCell("Imported date");
            for (Product p : result) {
            table.addCell(Integer.toString(p.getId()));
            table.addCell(p.getName());
            table.addCell(Double.toString(p.getUnitPrice()));
            table.addCell(Integer.toString(p.getQty()));
            table.addCell(p.getDate());
            }
            System.out.println(table.render());


    }

    public static void save(Connection c, ArrayList<Product> productList){
        AccessDatabase.deleteAll(c, "product");
        for (Product p : productList) {
            AccessDatabase.insertData(c, p, "product");
        }
        System.out.println("Being Add");
        System.out.println("Save Successfully");
        AccessDatabase.deleteAll(c, "temp");
    }
}