package com.company;

import java.util.Scanner;

public class Validator {

    private static Scanner reader = new Scanner(System.in);
    //VALIDATION FUNCTION INPUT BY INTEGER
    public static Integer inputNum(Scanner sc, String st) {
        System.out.print(st);
        if (sc.hasNextInt()) {
            int number = sc.nextInt();
            sc.nextLine();
            return number;
        } else {
            System.out.print("\n ~~~~~~~~~ Invalid Input! Only Numbers are Allowed. ~~~~~~~~~ \n");
            return inputNum(new Scanner(System.in), st);
        }
    }

    //Validate floating number
    public static Double inputDouble(Scanner sc, String st) {
        System.out.print(st);
        if (sc.hasNextDouble()) {
            Double number = sc.nextDouble();
            sc.nextLine();
            return number;
        } else {
            System.out.print("\n ~~~~~~~~~ Invalid Input! Only Numbers are Allowed. ~~~~~~~~~ \n");
            return inputDouble(new Scanner(System.in), st);
        }
    }

    //validate yes &  no answer
    public static String inputYesNo(Scanner sc, String st) {
        System.out.print(st);
        if (sc.hasNextLine()) {
            String input = sc.nextLine();
            if(input.toLowerCase().equals("y") || input.toLowerCase().equals("n"))
                return input;
            else {
                System.out.print("\n ~~~~~~~~~ Invalid Input! Only 'Y/y' or 'N/n' are Allowed. ~~~~~~~~~ \n");
                return inputYesNo(new Scanner(System.in), st);
            }
        }
        return st;
    }
}
