package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pagination<T>{
    private static int DEFAULT_PAGE_SIZE = 3;
    private List<T> list;
    private List<List<T>> listOfPages;
    private int pageSize = DEFAULT_PAGE_SIZE;
    private int currentPage = 0;
    public Pagination(List<T> list) {
        this.list = list;
        initPages();
    }
    public Pagination(List<T> list, int pageSize) {
        this.list = list;
        this.pageSize = pageSize;
        initPages();
    }
    public List<T> getPage(int pageNumber) {
        if (listOfPages == null ||
                pageNumber > listOfPages.size() ||
                pageNumber < 1) {
            return Collections.emptyList();
        }
        currentPage = pageNumber;
        List<T> page = listOfPages.get(--pageNumber);
        return page;
    }
    public void initPages() {
        if (list == null || listOfPages != null) {
            return;
        }

        if (pageSize <= 0 || pageSize > list.size()) {
            pageSize = list.size();
        }

        int numOfPages = (int) Math.ceil((double) list.size() / (double) pageSize);
        listOfPages = new ArrayList<List<T>>(numOfPages);
        for (int pageNum = 0; pageNum < numOfPages;) {
            int from = pageNum * pageSize;
            int to = Math.min(++pageNum * pageSize, list.size());
            listOfPages.add(list.subList(from, to));
        }
    }
    public int numberOfPages() {
        if (listOfPages == null) {
            return 0;
        }

        return listOfPages.size();
    }
    public List<T> nextPage() {
        List<T> page = getPage(++currentPage);
        return page;
    }
    public List<T> displayHome(){
        List<T> homePage = getPage(1);
        return homePage;
    }
    public List<T> previousPage() {
        List<T> page = getPage(--currentPage);
        return page;
    }
    public List<T> lastPage(){
        List<T> page = getPage(numberOfPages());
        return page;
    }
    public List<T> firstPage(){
        List<T> homePage = getPage(1);
        return homePage;
    }
    public int getCurrentPage(){
        return currentPage;
    }
    public void setRow(int setrow){
        pageSize=setrow;
    }
    public List<T> goTo(int goPage) {
        List<T> go = getPage(goPage);
        return go;
    }
}
