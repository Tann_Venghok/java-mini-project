package com.company;

import model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

public class DeleteAndUpdate {
    private static Scanner reader = new Scanner(System.in);

    public static void delete(ArrayList<Product> productList){
        checkIfEmpty(productList);
        int id = Validator.inputNum(new Scanner(System.in), "Delete by ID : ");
        Product p = Display.searchByID(id, productList);

        checkIfNull(p);

        String input = Validator.inputYesNo(new Scanner(System.in),
                "Do you want to Delete this from your record? [Y/y] or [N/n] : ");
        if(input.toLowerCase().equals("y")){
            productList.remove(p);
            System.out.println(p.getId() + " was removed successfully");
        }
    }

    public static void checkIfNull(Product p) {
        if(p != null){
            Display.displaySingleItem(p);
        }
        else {
            System.out.println("This product doesn't exist.");
            Display.menu();
            Display.chooseOpt();
        }
    }

    public static void checkIfEmpty(ArrayList<Product> productList) {
        if(productList.isEmpty()){
            System.out.println("\\n ~~~~~~~~~ Sorry! There is 0 Item to remove :( ~~~~~~~~~ \\n");
            Display.menu();
            Display.chooseOpt();
        }
    }

    public static void update(ArrayList<Product> productList){
        checkIfEmpty(productList);
        int id = Validator.inputNum(new Scanner(System.in),"Please Input Product's ID : " );
        Product p = Display.searchByID(id, productList);

        checkIfNull(p);

        System.out.println("What do you want to update?");
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.center, CellStyle.AbbreviationStyle.crop,
                CellStyle.NullStyle.emptyString);
        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX_WIDE, ShownBorders.ALL, false, "");
        t.addCell("1. All    2. Name     3. Quantity    4. Unit Price   5. Back to Menu", cs);
        System.out.println(t.render());

        System.out.print("Option(1-5) : ");
        int input = Integer.parseInt((reader.nextLine()));
        if(input == 1){
            updateAll(p, productList);
        } else  if(input == 2){
            updateName(p, productList);
        }else if(input == 3){
            updateUnitprice(p, productList);
        }else if(input == 4){
            updateQty(p, productList);
        }else if(input == 5){
            Display.menu();
            Display.chooseOpt();
        }else{
            System.out.println("Please Input a Valid Option(1-5)");
        }
    }

    private static void updateQty(Product p, ArrayList<Product> productList) {

        int qty = Validator.inputNum(new Scanner(System.in),"Product's Qty : " );
        Product newProduct = new Product(p.getId(), p.getName(), p.getUnitPrice(), qty, p.getDate());
        Display.displaySingleItem(newProduct);
        confirmMethod(p, newProduct, productList);
    }

    private static void updateUnitprice(Product p, ArrayList<Product> productList) {
        double unitPrice = Validator.inputDouble(new Scanner(System.in),"Product's UnitPrice : " );
        Product newProduct = new Product(p.getId(), p.getName(), unitPrice, p.getQty(), p.getDate());
        Display.displaySingleItem(newProduct);

        confirmMethod(p, newProduct, productList);
    }

    private static void updateName(Product p, ArrayList<Product> productList) {
        System.out.print("Product's Name : ");
        String name =reader.nextLine();

        Product newProduct = new Product(p.getId(), name, p.getUnitPrice(), p.getQty(), p.getDate());
        Display.displaySingleItem(newProduct);
        confirmMethod(p, newProduct, productList);
    }

    private static void updateAll(Product p, ArrayList<Product> productList) {
        System.out.print("Product's Name : ");
        String name =reader.nextLine();
        Double unitPrice = Validator.inputDouble(new Scanner(System.in), "Product's UnitPrice : ");
        int qty = Validator.inputNum(new Scanner(System.in), "Product's Qty : ");

        Product newProduct = new Product(p.getId(), name, unitPrice, qty, p.getDate());
        Display.displaySingleItem(newProduct);

        confirmMethod(p, newProduct, productList);
    }

    public static void confirmMethod(Product p, Product newProduct, ArrayList<Product> productList) {
        String input = Validator.inputYesNo(new Scanner(System.in),
                "Do you want to update this to your record? [Y/y] or [N/n] : ");
        if(input.toLowerCase().equals("y")){
            int i = productList.indexOf(p);
            productList.set(i, newProduct);
            System.out.println(p.getId() + " was update successfully");
        }
    }

}
