package com.company;

import Database.AccessDatabase;
import model.Product;

import java.io.*;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;

public class Bonus {
    private static String getCurrentDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-HHmmss");
        Date date = new Date();
        return (formatter.format(date));
    }
    static String filename="";
    //specify the path (folder) where you want to search files
    private static final String fileLocation = "D:\\Java mini project\\src\\com\\company\\Backupfile\\"+filename;

    //extension you want to search for e.g. .png, .jpeg, .xml etc
    private static final String searchThisExtn = ".txt";

    //check temp table
    public static boolean checkIfTempNull(Connection c){
        ArrayList<Product> temp = AccessDatabase.getDataIntoArray(c, "temp");
        if(temp.isEmpty())
            return true;
        return false;
    }

    //check temp array
    static void checkIfTempArrayNull(ArrayList<Product> temp) {
        String tb = "temp";
        if (temp != null){
            AccessDatabase.deleteAll(Display.c, tb);
            for (Product p:temp) {
                AccessDatabase.insertData(Display.c, p, tb);
            }
        }
    }

    public static void backUp(ArrayList<Product> pro) {
        String nameFile = getCurrentDate();
        File file = new File("D:\\Java mini project\\src\\com\\company\\Backupfile\\" + nameFile + ".txt");
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < pro.size(); i++) {
                bufferedWriter.write(pro.get(i).getId() + "," + pro.get(i).getName() + "," + pro.get(i).getUnitPrice() + "," + pro.get(i).getQty() + "," + pro.get(i).getDate());
                bufferedWriter.newLine();
            }
            System.out.println("backup succesfully in " + getCurrentDate());
            bufferedWriter.close();

        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void restore(ArrayList<Product> pro) {
        Product p = new Product();
        Bonus bo = new Bonus();
        int index=1;
        String file = bo.listFiles(fileLocation,searchThisExtn);
        if(file!=""){
            String st[] = file.split(",");
            for(String str : st){
                System.out.println((index++)+") "+str);
            }
            int numFile = Validator.inputNum(new Scanner(System.in),"Choose File:");
            if(numFile<st.length&&numFile>st.length){
                System.out.println("Can't find file");
            }else{
                for(int i=0;i<st.length;i++){
                    if((numFile-1)==i){
                        filename=st[i];
                    }
                }
                try{
                    String strs;
                    File newFile = new File("D:\\Java mini project\\src\\com\\company\\Backupfile\\"+filename);
                    BufferedReader buff =new BufferedReader(new FileReader(newFile));
                    while((strs=buff.readLine())!=null){
                        String sts[] = strs.split(",");
                        pro.add(new Product(Integer.parseInt(sts[0]),sts[1],Double.parseDouble(sts[2]),Integer.parseInt(sts[3]),sts[4]));
                    }
                    System.out.println("Restore Succesfully");
                    buff.close();
                }catch (IOException e){
                    System.out.println(e);
                }
            }
        }
    }

    public String listFiles(String loc, String extn) {

        SearchFile files = new SearchFile(extn);

        File folder = new File(loc);

        if (folder.isDirectory() == false) {
            System.out.println("Folder does not exists: " + fileLocation);
        }

        String[] list = folder.list(files);
        if (list.length == 0) {
            System.out.println("There are no files to restore");
        }
        String Mfile="";
        for (String file : list) {
            String temp = new StringBuffer(file).toString();
            Mfile+=temp+",";
        }
        return Mfile;
    }

}
