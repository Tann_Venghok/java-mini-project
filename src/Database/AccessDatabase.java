package Database;

import model.Product;

import java.sql.*;
import java.util.ArrayList;

public class AccessDatabase {
    static PreparedStatement ps;
    static Statement st;

    public static void deleteAll(Connection c, String tb){
        try {
            st = c.createStatement();
            st.executeUpdate("DELETE  FROM " + tb);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteData(Connection c, int id) {
        try {
            ps = c.prepareStatement("DELETE FROM product WHERE ID = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertIntoOtherTable(Connection c, String tb1, String tb2){
        try {
            ps = c.prepareStatement("INSERT INTO " + tb1 +
                    " SELECT * FROM " + tb2);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertData(Connection c, Product p, String tbName) {
        try {
            ps = c.prepareStatement("INSERT INTO " + tbName + " VALUES (?,?,?,?,?)");
            ps.setInt(1, p.getId());
            ps.setString(2, p.getName());
            ps.setDouble(3, p.getUnitPrice());
            ps.setInt(4, p.getQty());
            ps.setString(5, p.getDate());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Product> getDataIntoArray(Connection c, String tb) {
        ArrayList<Product> list = new ArrayList<Product>();

        try {
            st = c.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM " + tb);

            while (rs.next()) {
                int id = rs.getInt("ID");
                String name = rs.getString("Name");
                double unitPrice = rs.getDouble("Unit Price");
                int qty = rs.getInt("Qty");
                String date = rs.getString("Imported Date");

                Product p = new Product(id, name, unitPrice, qty, date);
                list.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }
}
