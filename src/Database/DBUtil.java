package Database;

import java.sql.*;

public class DBUtil {
    public static Connection connectDB(){
        Connection c = null;
        try{
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test_db",
                    "postgres", "12345");
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }

    public static void closeConnection(ResultSet rs, PreparedStatement ps, Connection c){
        try {
            rs.close();
            ps.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

